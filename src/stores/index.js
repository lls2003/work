import { reactive } from 'vue'
import { defineStore } from 'pinia'

export const useCategoryStore = defineStore('Categary', () => {
  const classLists=reactive({
    classes:[]
  })
  function addClass1(val){
    classLists.classes.unshift(val)
  }
  function deleteClass(index){
    classLists.classes.splice(index,1)
  }

  return { classLists,addClass1,deleteClass }
})
export const useNoteStore = defineStore('Note', () => {
  const NoteLists=reactive({
    Notes:[]
  })
  function deleteNote(index){
    NoteLists.Notes.splice(index,1)
  }
  return {NoteLists,deleteNote}
})

