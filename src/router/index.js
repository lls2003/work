import { createRouter, createWebHistory } from 'vue-router'
import login from '../components/login.vue'
import register from '../components/register.vue'
import home from '../components/home.vue'
import mainPart from '../components/mainPart.vue'
import homePage from '../components/homePage.vue'
import newNote from '../components/newNote.vue'

const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      path:"/",
      component:login
    },
    {
      path:"/register",
      component:register
    },
    {
      path:"/home",
      component:home,
      redirect:"/home/homePage",
      children:[
        {
          path:"homePage",
          component:homePage
        },
        {
          path:"mainPart",
          component:mainPart
        },
        {
          path:"newNote",
          component:newNote
        }
    ]
    }
  ]
})

export default router;